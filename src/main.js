const electron = require('electron')
    // Module to control application life.

const env = require('dotenv').config()

if (env.error) {
    throw env.error
}

// Enable live reload for all the files inside your project directory
const ignoredResources = /resources|[\/\\]\./
const ignoredEventFile =  /evt|[\/\\]\./
const ignoredAssets = /assets|[\/\\]\./
// require('electron-reload')(__dirname, {ignored: /resources|[\/\\]\./});

if (process.env.NODE_ENV !== 'production'){
    require('electron-reload')(__dirname, {ignored: [ignoredResources, ignoredEventFile, ignoredAssets] });   
}

const app = electron.app
    // Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

const path = require('path')
const url = require('url')



// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
//$("#quit").addEventListener('click',quit_button);


function createWindow() {
    // Create the browser window.

    mainWindow = new BrowserWindow({
        transparent: false,
        fullscreen: false,
        frame: true,
        width: 1200,
        height: 800,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            preload: path.join(__dirname, 'preload.js'),
            devTools: (process.env.ELECTRON_ENABLE_LOGGING==="true"?true:false)
            
        }
    });

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }))
    if(process.env.ELECTRON_DEBUG === "true"){
        // Open the DevTools.
        mainWindow.webContents.openDevTools()
    }
    // Emitted when the window is closed.
    mainWindow.on('closed', function() {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function() {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    app.quit()
})

app.on('activate', function() {

    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

function quit_button(){
    app.quit()
}

var ipc = require('electron').ipcMain;
const os = require('os')
var { dialog } = require('electron');
const fs = require('fs');

const fileProperties = {
    'linux':['openfile'],
    'win32':['openfile'],
    'mac':['openfile','openDirectory']
}

ipc.on('invokeAction', function(event){
    quit_button()
});

// Retrieve the event that is send by the renderer process
ipc.on('open-file-dialog-for-file',function(event){
    dialog.showOpenDialog({
        properties:fileProperties[os.platform],
        filters: [
            { extensions: ['jpeg','jpg', 'png', 'gif'] },
        ]
    }).then(function(response){
        if (!response.canceled) {
            // handle fully qualified file name
            console.log(response.filePaths[0]);
        } else {
            console.log("no file selected");
        }
        event.sender.send('selected-file',response.filePaths[0])
    })
})
