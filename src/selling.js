const ipcRenderer = require('electron').ipcRenderer
const path = require('path')
const fs = require('fs');

let tempImage

/*----------------------------------CLASS PRODUCT ----------------------------------*/
class Product {
    constructor(image,name,barcode,price) {
        this.image = image
        this.name = name
        this.barcode = barcode
        this.price = price
    }
}
  
class Products {
    constructor(){
        this.product_list = []
    }
    
    newProduct(image,name,barcode,price){
        let p = new Product(image,name,barcode,price)
        this.product_list.push(p)
        return p
    }

    checkBarcode(barcode){
        var result = false
        if(this.product_list.length > 0){
            for(let index = 0;index < this.product_list.length;index++){
                if(this.product_list[index].barcode === barcode){
                    result = true
                    break
                }
            }
        }
        return result
    }

    GetProduct(barcode){
        var product = []
        if(this.product_list.length > 0){
            for(let index = 0;index < this.product_list.length;index++){
                if(this.product_list[index].barcode === barcode){
                    product = {
                        "name" : this.product_list[index].name,
                        "price" : this.product_list[index].price
                    }
                    break
                }
            }
        }
        return product
    }
        
    get allProduct(){
      return this.product_list
    }
    
    get lengthOfProduct(){
        return this.product_list.length
    }
}

 
/*----------------------------------CLASS CART LIST ----------------------------------*/
class CartList {
    constructor(name,barcode,price,amount) {
        this.name = name
        this.barcode = barcode
        this.price = price
        this.amount = amount
    }
}

class CartLists {
    constructor(){
        this.in_cart = []
    }
    
    insertProduct(name,barcode,price,amount){
        let p = new CartList(name,barcode,price,amount)
        this.in_cart.push(p)
        return p
    }

    checkBarcode(barcode){
        var cart = {
            result:false
        }
        if(this.in_cart.length > 0){
            for(let index = 0;index < this.in_cart.length;index++){
                if(this.in_cart[index].barcode === barcode){
                    cart = {
                        result:true,
                        index:index
                    }
                    break
                }
            }
        }
        return cart
    }
    
    deleteProduct(id){
        (this.in_cart).splice(id,1)
    }

    get allProduct(){
        return this.in_cart
    }

    get lengthOfProduct(){
        return this.in_cart.length
    }
}

/*----------------------------------CLASS PRODUCT ----------------------------------*/

class Report {
    constructor(name,barcode,price,amount) {
        this.name = name
        this.barcode = barcode
        this.price = price
        this.amount = amount
    }
}

class Reports {
    constructor(){
        this.report_list = []
    }
    
    insertReport(name,barcode,price,amount){
        let p = new Report(name,barcode,price,amount)
        this.report_list.push(p)
        return p
    }

    checkBarcode(barcode){
        var cart = {
            result:false
        }
        if(this.report_list.length > 0){
            for(let index = 0;index < this.report_list.length;index++){
                if(this.report_list[index].barcode === barcode){
                    cart = {
                        result:true,
                        index:index
                    }
                    break
                }
            }
        }
        return cart
    }

    get allProduct(){
        return this.report_list
    }

    get lengthOfProduct(){
        return this.report_list.length
    }

}



/*----------------------------------------------------------------------------------*/



const btn = document.getElementsByTagName("button")
const dialog_popup = document.getElementById("dialog")
const dialog_alert = document.getElementById("dialog-alert")
const dialog_checkout = document.getElementById("dialog-checkout")

let product = new Products()
let cart = new CartLists()
let report = new Reports()
var page_status
const vat = 7

document.getElementById("new-product").addEventListener("click",newProductPopup)

$(document).ready(()=>{
    pageRespons("main")
    init()
})


function init(){
    for(let index = 0;index < btn.length;index++){
        btn[index].addEventListener("click",loadPage,false)
    }
}

function loadPage(){
    if((this.id).search("btn") === 0){
        const page = (this.id).replace("btn-","")
        pageRespons(page)
    }
}

function pageRespons(page){
    page_status = page
    $.ajax({
        url:`page/${page}.html`,
        success(res){
            document.getElementById("page-respons").innerHTML = res
            if(page === "main"){
                document.getElementById("barcode-input").addEventListener("keyup",barcodeRead,false)
                document.getElementById("receive-input").addEventListener("keyup",calculatorChange,false)
                document.getElementById("receive-input").addEventListener("click",clearBox,false)
                document.getElementById("check-out").addEventListener("click",checkOut,false)
                cartInsert()
            }else if(page === "product"){
                ProductList()
            }else if(page === "report"){
                ReportList()
            }
        }
    })
}

/*----------------------------------------------------------------------------------*/

function barcodeRead(event){
    if(event.keyCode === 13){
        var barcode = document.getElementById("barcode-input").value
        if(product.checkBarcode(barcode) === true){
            var prdItem = product.GetProduct(barcode)
            var chkCart = cart.checkBarcode(barcode)
            if(chkCart.result === false){
                cart.insertProduct(prdItem.name,barcode,prdItem.price,1)
            }else{
                cart.in_cart[chkCart.index].amount += 1
            }
                      
            cartInsert()
        }
    }
}

function cartInsert(){
    if(cart.lengthOfProduct > 0){
        var cart_list = document.getElementById("cart-list")
        var total_net = 0

        cart_list.innerHTML = ""

        for(let index = 0;index < cart.lengthOfProduct;index++){
            var card_prd = document.createElement("div"),
                row_prd = document.createElement("div"),
                input_amt = document.createElement("input")

            var prd_name = document.createElement("div"),
                prd_price = document.createElement("div"),
                prd_amt = document.createElement("div"),
                prd_total_price = document.createElement("div"),
                prd_delete = document.createElement("div")
            
            var icon_bin = document.createElement("i")
            
                // card product-car
            card_prd.setAttribute("class","card product-cart")
            row_prd.setAttribute("class","row")
            prd_name.setAttribute("class","col-4")
            prd_price.setAttribute("class","col-2")
            prd_amt.setAttribute("class","col-3")
            prd_total_price.setAttribute("class","col-2")
            prd_delete.setAttribute("class","col-1")

            var amt = cart.in_cart[index].amount
            var total_price = amt * cart.in_cart[index].price

            input_amt.setAttribute("id",`amount-${index}`)
            input_amt.setAttribute("class","form-control")
            input_amt.setAttribute("style","text-align:center;height:25px")
            input_amt.addEventListener("keyup",calculatorAmount,false)
            input_amt.value = amt

            prd_name.innerHTML = cart.in_cart[index].name
            prd_price.innerHTML = cart.in_cart[index].price
            prd_amt.appendChild(input_amt)
            prd_total_price.innerHTML = total_price

            icon_bin.setAttribute("id",`delete-${index}`)
            icon_bin.setAttribute("class","fas fa-trash")
            icon_bin.addEventListener("click",deleteProduct,false)

           
            prd_delete.appendChild(icon_bin)

            row_prd.appendChild(prd_name)
            row_prd.appendChild(prd_price)
            row_prd.appendChild(prd_amt)
            row_prd.appendChild(prd_total_price)
            row_prd.appendChild(prd_delete)
            card_prd.appendChild(row_prd)
            cart_list.appendChild(card_prd)

            
            total_net += total_price
        }
        document.getElementById("before-vat-res").innerHTML = beforeVat(total_net).toFixed(2)
        document.getElementById("vat-res").innerHTML = `${vat} %`
        document.getElementById("total-vat-res").innerHTML = total_net.toFixed(2)
        document.getElementById("net-total").innerHTML = total_net.toFixed(2)
        calculatorChange()
    }
}

function calculatorChange(){
    var receive = document.getElementById("receive-input").value,
        total_net = parseFloat(document.getElementById("total-vat-res").innerHTML).toFixed(2)
        
    document.getElementById("change-res").innerHTML = (receive - total_net).toFixed(2)
}

function clearBox(){
    document.getElementById("receive-input").value = ""
}

function beforeVat(price){
    return price/(1+(vat/100))
}

function calculatorAmount(e){
    if(e.keyCode === 13){
        var id = `${(this.id).replace("amount-","")}`
        cart.in_cart[id].amount = document.getElementById(`amount-${id}`).value
        cartInsert()
        document.getElementById(this.id).focus()
    }
}

/*----------------------------------------------------------------------------------*/

function newProductPopup(){
    dialog_popup.style.display = "inline-block"
    document.getElementById("cancel-product").addEventListener("click",CancelAddProduct)
    document.getElementById("add-product").addEventListener("click",AddProductList)
    document.getElementById("upload-product").addEventListener('click',(event)=>{
        ipcRenderer.send('open-file-dialog-for-file')
    });
    document.getElementById("product-name-input").focus()
}

function deleteProduct(){
    dialog_alert.style.display = "inline-block"
    document.getElementById("product-id").innerHTML = `${(this.id).replace("delete-","")}`
    document.getElementById("cancel-delete").addEventListener("click",CancelAlert)
    document.getElementById("delete-product").addEventListener("click",DeleteProductList)
}

function checkOut(){
    if(cart.lengthOfProduct > 0){
        dialog_checkout.style.display = "inline-block"
        document.getElementById("product-id").innerHTML = `${(this.id).replace("delete-","")}`
        document.getElementById("cancel-checkout").addEventListener("click",CancelCheckout)
        document.getElementById("checkout-product").addEventListener("click",checkOutProduct)
    }
  
}

function AddProductList(){
    const image = SaveImageToProductList()
    const name = document.getElementById("product-name-input").value
    const barcode = document.getElementById("barcode-product-input").value
    const price = document.getElementById("price-input").value
    if(product.checkBarcode(barcode) === false){
        product.newProduct(image,name,barcode,price)
    }else{
        console.log("barcode same in list")
    }

    CancelAddProduct()

    if(page_status == "product"){
        ProductList()
    }
}

function DeleteProductList(){
    var prd_id = document.getElementById("product-id").innerHTML

    if(page_status === "main"){
        cart.deleteProduct(prd_id)

        CancelAlert()
        cartInsert()
    
        if(cart.lengthOfProduct == 0){
            pageRespons("main")
        }
    }else if(page_status === "product"){
        product.product_list.splice(prd_id,1)
        console.log("deleteList")
        CancelAlert()
        pageRespons("product")
    }
}


function checkOutProduct(){
    for(let index = 0;index < cart.lengthOfProduct;index++){
        report.insertReport(cart.in_cart[index].name,cart.in_cart[index].barcode,cart.in_cart[index].price,cart.in_cart[index].amount)
    }
    cart.in_cart = []
    CancelCheckout()
    pageRespons("main")
}

function CancelAddProduct(){
    dialog_popup.style.display = "none"
    document.getElementById("product-name-input").value = ""
    document.getElementById("barcode-product-input").value = ""
    document.getElementById("price-input").value = ""
    
}

function CancelAlert(){
    dialog_alert.style.display = "none"    
}

function CancelCheckout(){
    dialog_checkout.style.display = "none"    
}

/*----------------------------------------------------------------------------------*/

function ProductList(){
    if(product.lengthOfProduct > 0){
        var prd_respons_list = document.getElementById("product-list")
        prd_respons_list.innerHTML = ""

        const table = document.createElement("table"),
            tr_th = document.createElement("tr"),
            th_image = document.createElement("th"),
            th_name = document.createElement("th"),
            th_barcode = document.createElement("th"),
            th_price = document.createElement("th"),
            th_delete = document.createElement("th")
        
        th_image.innerHTML = "Image"
        th_name.innerHTML = "Name"
        th_barcode.innerHTML = "Barcode"
        th_price.innerHTML = "Price"

        th_name.setAttribute("width","30%")
        th_barcode.setAttribute("width","30%")
        th_price.setAttribute("width","20%")

        tr_th.appendChild(th_image)
        tr_th.appendChild(th_name)
        tr_th.appendChild(th_barcode)
        tr_th.appendChild(th_price)
        tr_th.appendChild(th_delete)

        table.appendChild(tr_th)

        for(let index = 0;index < product.lengthOfProduct;index++){
            var tr_td = document.createElement("tr"),
                td_image = document.createElement("td"),
                td_name = document.createElement("td"),
                td_barcode = document.createElement("td"),
                td_price = document.createElement("td"),
                td_delete = document.createElement("td")
            
            var icon_delete = document.createElement("i")
            
            td_image.innerHTML = `<img src="..\\${(product.product_list[index].image)}" class = "img-thumbnail" style="width: 75px;" />`
            td_name.innerHTML = product.product_list[index].name
            td_barcode.innerHTML = product.product_list[index].barcode
            td_price.innerHTML = parseFloat(product.product_list[index].price).toFixed(2)

            td_price.setAttribute("align","right")

            icon_delete.setAttribute("id",`delete-${index}`)
            icon_delete.setAttribute("class","fas fa-trash")
            icon_delete.addEventListener("click",deleteProduct)

            td_delete.appendChild(icon_delete)

            tr_td.appendChild(td_image)
            tr_td.appendChild(td_name)
            tr_td.appendChild(td_barcode)
            tr_td.appendChild(td_price)
            tr_td.appendChild(td_delete)

            table.appendChild(tr_td)
        } 

        table.setAttribute("class","product-list-table")
        prd_respons_list.appendChild(table)
    }
}


function ReportList(){
    var total_net = 0
    if(report.lengthOfProduct > 0){
        var report_res_list = document.getElementById("report-list")
        report_res_list.innerHTML = ""

        const table = document.createElement("table"),
            tr_th = document.createElement("tr"),
            th_name = document.createElement("th"),
            th_barcode = document.createElement("th"),
            th_price = document.createElement("th"),
            th_amount = document.createElement("th"),
            th_total = document.createElement("th")
        
        th_name.innerHTML = "Name"
        th_barcode.innerHTML = "Barcode"
        th_amount.innerHTML = "Amount"
        th_price.innerHTML = "Price"
        th_total.innerHTML = "Total"

        th_name.setAttribute("width","30%")
        th_barcode.setAttribute("width","20%")
        th_price.setAttribute("width","20%")
        th_amount.setAttribute("width","10%")
        th_total.setAttribute("width","20%")

        tr_th.appendChild(th_name)
        tr_th.appendChild(th_barcode)
        tr_th.appendChild(th_price)
        tr_th.appendChild(th_amount)
        tr_th.appendChild(th_total)

        table.appendChild(tr_th)

        for(let index = 0;index < report.lengthOfProduct;index++){
            var tr_td = document.createElement("tr"),
                td_name = document.createElement("td"),
                td_barcode = document.createElement("td"),
                td_price = document.createElement("td"),
                td_amount = document.createElement("td"),
                td_total = document.createElement("td")
            
            var price = parseFloat(report.report_list[index].price).toFixed(2),
                amount = parseInt(report.report_list[index].amount),
                total = (price * amount).toFixed(2)

            td_name.innerHTML = report.report_list[index].name
            td_barcode.innerHTML = report.report_list[index].barcode
            td_price.innerHTML = price
            td_amount.innerHTML = amount
            td_total.innerHTML = total

            td_price.setAttribute("align","right")


            tr_td.appendChild(td_name)
            tr_td.appendChild(td_barcode)
            tr_td.appendChild(td_price)
            tr_td.appendChild(td_amount)
            tr_td.appendChild(td_total)

            table.appendChild(tr_td)

            total_net += total
        } 

        table.setAttribute("class","product-list-table")
        report_res_list.appendChild(table)
        document.getElementById("total-net-report").innerHTML = parseFloat(total_net).toFixed(2)
    }
}

function SaveTempImageFile(srcPath){
    var fileName = `product-item-${new Date().getTime()}`
    var destPath = path.join(`resources/images/${fileName}.jpg`)
    fs.copyFile(srcPath , destPath, (err) => {
        if (err) {
            console.log("Error Found:", err);
        }else{
            
        }
    });
    return destPath
}

function SaveImageToProductList(){
    var fileName = `product-item-${new Date().getTime()}`
    var destPath = path.join(`assets/product-items/${fileName}.jpg`)
    fs.copyFile(tempImage , destPath, (err) => {
        if (err) {
            console.log("Error Found:", err);
        }else{
        }
        // document.getElementById('product-image-input').src = `../assets/empty_photo-400x300.png`
    });
    return destPath
}

// IPC Event Listener
ipcRenderer.on('selected-file',function(event,path){
    document.getElementById('product-image-input').src = path
    tempImage = SaveTempImageFile(path)
})
