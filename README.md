# POS - Offline

## Prepare project for Test and Deploy

****This step support Windows 10 only**

Things to prepare before testing

| Tools | Version | Description |
| --- | --- | --- |
| chocolatey | [0.10.15 Up](https://chocolatey.org/install) | Chocolatey is software management automation for Windows that wraps installers, executables, zips, and scripts into compiled packages |
| Gitlab Runner | [14.6.0 Up](https://gitlab.com/archives1/ci/gitlab-runner.git) | using for make CI/CD |
| nodejs | [17.3.0 Up](https://community.chocolatey.org/packages/nodejs) | install package by npm command |
| npm | [8.3.0 Up](https://docs.npmjs.com/cli/v8/commands/npm-install) | install some package for this project |
| python3 | [3.9.10 Up](https://www.python.org/downloads/release/python-3910/) | install some package for build desktop windows app |
| pip | [21.2.4 Up](https://www.python.org/downloads/release/python-3910/) | install some package for build desktop windows app |



### Run App Development mode

```cmd
npm start
```

### Run unit testing

```cmd
npm test
```

### Build App via electron-packager

```cmd
npx electron-packager . electron-js-quick-start --platform=win32 --arch=x64
```

## hint: install chocolatey first!

### could be that visualstudio2019-workload-vctools is enough

Run cmd as Adminstrator

```cmd
choco install -y visualstudio2019buildtools visualstudio2019-workload-vctools
npm config set msvs_version 2019
```

### Install python3 via Chocolatey Software

```cmd
choco install python3 --version=3.9.9
```

Add System variables section `PATH`

Excursus: Setting environment variables

Windows allows environment variables to be configured permanently at both the User level and the System level, or temporarily in a command prompt.

To temporarily set environment variables, open Command Prompt and use the set command:

```cmd
setx PATH=C:\Program Files\Python39;C:\Program Files\Python39\Scripts;%PATH%
setx PYTHONPATH=%PYTHONPATH%;C:\Program Files\Python39\Lib
setx PYTHON=C:\Program Files\Python39\Python.exe
setx PYTHON3=C:\Program Files\Python39\Python.exe

```



Select env file

```cmd
# Development
setx ENV_FILENAME .env.development
copy %ENV_FILENAME% .env

# Production
setx ENV_FILENAME .env.production
copy %ENV_FILENAME% .env

```
### Build all App via electron-builder

```cmd
# Before build
copy %ENV_FILENAME% .env

# Build
make build_all_win

# After build
copy %ENV_FILENAME% dist\\win-unpacked\\.env
copy %ENV_FILENAME% dist\\.env
```






